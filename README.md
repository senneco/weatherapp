# Weather app
Show current weather and forecast from http://openweathermap.org/. You have opportunity to add some city, or remove by swipe.

## Build dependencies

- Android SDK 23+
- JDK 7+
- Android build tools 21.1.2

## Builds

[Latest build result](https://bitbucket.org/senneco/weatherapp/raw/6db2bfeee3fd267f4f0dfa292d8e4cee8f71a4f8/app/build/outputs/apk/app-debug.apk)

## How-To Build


```
#!

// Go to root project directory
$ cd WeatherApp
 
// Add to root dir file local.properties, that contains sdk.dir=<path to Android SDK>,
// Or add ANDROID_HOME to enviropment variables

// Pull gradle and some dependensies
$ gradlew

// Create debug .apk file inside the build/ directory
$ gradlew assembleDebug
```