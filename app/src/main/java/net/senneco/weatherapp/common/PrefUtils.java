package net.senneco.weatherapp.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by senneco on 11.03.2016
 */
public class PrefUtils
{
	public static SharedPreferences getPrefs(Context context)
	{
		return PreferenceManager.getDefaultSharedPreferences(context);
	}

	public static SharedPreferences.Editor getEditor(Context context)
	{
		return getPrefs(context).edit();
	}
}
