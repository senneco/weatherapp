package net.senneco.weatherapp.app;

import android.app.Application;

import net.senneco.weatherapp.di.component.AppComponent;
import net.senneco.weatherapp.di.component.DaggerAppComponent;
import net.senneco.weatherapp.di.component.DaggerStorageComponent;
import net.senneco.weatherapp.di.component.StorageComponent;
import net.senneco.weatherapp.di.module.AppModule;
import net.senneco.weatherapp.di.module.CityModule;
import net.senneco.weatherapp.di.module.WeatherModule;
import net.senneco.weatherapp.mvp.model.storage.CityStorage;
import net.senneco.weatherapp.mvp.model.storage.WeatherStorage;

import io.paperdb.Paper;

/**
 * Created by senneco on 10.03.2016
 */
public class WeatherApp extends Application
{
	private static AppComponent sAppComponent;
	private static StorageComponent sStorageComponent;

	@Override
	public void onCreate()
	{
		super.onCreate();

		sAppComponent = DaggerAppComponent.builder()
				.appModule(new AppModule(this))
				.build();
		sStorageComponent = DaggerStorageComponent.builder()
				.cityModule(new CityModule(new CityStorage()))
				.weatherModule(new WeatherModule(new WeatherStorage()))
				.build();

		Paper.init(this);
	}

	public static AppComponent getAppComponent()
	{
		return sAppComponent;
	}

	public static StorageComponent getStorageComponent()
	{
		return sStorageComponent;
	}
}
