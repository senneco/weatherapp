package net.senneco.weatherapp.mvp.model.network;

import net.senneco.weatherapp.mvp.model.network.model.ForecastResponse;
import net.senneco.weatherapp.mvp.model.network.model.WeatherResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by senneco on 11.03.2016
 */
public interface WeatherApi
{
	@GET("/data/2.5/weather?units=metric&mode=json&appid=cb2ae1bf2eac574aa8361242aeb5b460")
	Call<WeatherResponse> getWeather(@Query("q") String cityName);

	@GET("/data/2.5/weather?units=metric&mode=json&appid=cb2ae1bf2eac574aa8361242aeb5b460")
	Call<WeatherResponse> getWeather(@Query("lat") double lat, @Query("lon") double lon);

	@GET("/data/2.5/forecast?units=metric&mode=json&appid=cb2ae1bf2eac574aa8361242aeb5b460")
	Call<ForecastResponse> getForecast(@Query("q") String cityName);

	@GET("/data/2.5/forecast?units=metric&mode=json&appid=cb2ae1bf2eac574aa8361242aeb5b460")
	Call<ForecastResponse> getForecast(@Query("lat") double lat, @Query("lon") double lon);
}
