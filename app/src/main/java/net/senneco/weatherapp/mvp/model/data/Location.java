package net.senneco.weatherapp.mvp.model.data;

import org.parceler.Parcel;

/**
 * Class-wrapper with empty constructor. Required for Paper library
 *
 * Created by senneco on 12.03.2016
 */
@Parcel
public class Location
{
	private android.location.Location mLocation;

	// Required for paper
	public Location()
	{
	}

	public Location(android.location.Location location)
	{
		mLocation = location;
	}

	public double getLongitude()
	{
		return mLocation.getLongitude();
	}

	public double getLatitude()
	{
		return mLocation.getLatitude();
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Location location = (Location) o;

		return mLocation != null ? mLocation.equals(location.mLocation) : location.mLocation == null;
	}

	@Override
	public int hashCode()
	{
		return mLocation != null ? mLocation.hashCode() : 0;
	}
}
