package net.senneco.weatherapp.mvp.model.network.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by senneco on 11.03.2016
 */
public class WeatherResponse
{
	@SerializedName("dt")
	private long mDatestamp;
	@SerializedName("main")
	private Temp mTemp;

	public long getTimestamp()
	{
		return mDatestamp;
	}

	public Temp getTemp()
	{
		return mTemp;
	}
}
