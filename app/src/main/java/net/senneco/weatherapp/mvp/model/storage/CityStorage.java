package net.senneco.weatherapp.mvp.model.storage;

import java.util.ArrayList;
import java.util.List;

import net.senneco.weatherapp.mvp.model.data.City;

import io.paperdb.Paper;

/**
 * Created by senneco on 10.03.2016
 */
public class CityStorage
{
	public static final String CITIES = "cities";

	public List<City> getCites()
	{
		if (!Paper.book().exist("cities"))
		{
			List<City> cities = new ArrayList<>();
			cities.add(new City("London"));
			cities.add(new City("Tokyo"));
			cities.add(new City("New York"));

			Paper.book().write(CITIES, cities);

			return cities;
		}

		return Paper.book().read(CITIES);
	}

	public void remove(City city)
	{
		List<City> cities = getCites();
		cities.remove(city);

		Paper.book().write(CITIES, cities);
	}

	public void add(City city)
	{
		List<City> cities = getCites();
		cities.add(0, city);

		Paper.book().write(CITIES, cities);
	}
}
