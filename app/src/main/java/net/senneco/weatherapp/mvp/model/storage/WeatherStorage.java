package net.senneco.weatherapp.mvp.model.storage;

import net.senneco.weatherapp.mvp.model.data.City;
import net.senneco.weatherapp.mvp.model.data.Weather;

import io.paperdb.Paper;

/**
 * Created by senneco on 10.03.2016
 */
public class WeatherStorage
{
	public Weather getWeather(City city)
	{
		return Paper.book().read("weather_of_" + city, new Weather());
	}

	public void saveWeather(Weather weather)
	{
		Paper.book().write("weather_of_" + weather.getCity(), weather);
	}
}
