package net.senneco.weatherapp.mvp.model.network.model;

/**
 * Created by senneco on 11.03.2016
 */
public class Temp
{
	private double mTempMin;
	private double mTemp;
	private double mTempMax;

	public double getTempMin()
	{
		return mTempMin;
	}

	public double getTemp()
	{
		return mTemp;
	}

	public double getTempMax()
	{
		return mTempMax;
	}
}
