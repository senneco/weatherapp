package net.senneco.weatherapp.mvp.model.data;

import android.annotation.SuppressLint;

import org.parceler.Parcel;

/**
 * Created by senneco on 10.03.2016
 */
@Parcel
public class City
{
	private String mName;
	private Location mLocation;

	public City()
	{
	}

	public City(String name)
	{
		mName = name;
	}

	public String getName()
	{
		return mName;
	}

	public void setName(String name)
	{
		mName = name;
	}

	public void setLocation(android.location.Location location)
	{
		mLocation = new Location(location);
	}

	public Location getLocation()
	{
		return mLocation;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		City city = (City) o;

		if (mName != null ? !mName.equals(city.mName) : city.mName != null) return false;
		return mLocation != null ? mLocation.equals(city.mLocation) : city.mLocation == null;

	}

	@Override
	public int hashCode()
	{
		int result = mName != null ? mName.hashCode() : 0;
		result = 31 * result + (mLocation != null ? mLocation.hashCode() : 0);
		return result;
	}

	@SuppressLint("DefaultLocale")
	@Override
	public String toString()
	{
		return mName != null ? mName : mLocation != null ? String.format("%1$.2fx%2$.2f", mLocation.getLatitude(), mLocation.getLongitude()) : null;
	}
}
