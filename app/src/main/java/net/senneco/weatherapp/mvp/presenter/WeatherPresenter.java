package net.senneco.weatherapp.mvp.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import net.senneco.weatherapp.app.WeatherApp;
import net.senneco.weatherapp.mvp.common.WeatherAppSchedulers;
import net.senneco.weatherapp.mvp.model.data.City;
import net.senneco.weatherapp.mvp.model.repository.Repository;
import net.senneco.weatherapp.mvp.view.WeatherView;

import javax.inject.Inject;

/**
 * Created by senneco on 10.03.2016
 */
@InjectViewState
public class WeatherPresenter extends MvpPresenter<WeatherView>
{
	@Inject
	Repository mRepository;

	private City mCity;

	public WeatherPresenter()
	{
		WeatherApp.getAppComponent().inject(this);
	}

	public void init(City city)
	{
		mCity = city;
	}

	@Override
	protected void onFirstViewAttach()
	{
		super.onFirstViewAttach();

		loadData();
	}

	public void loadData()
	{
		getViewState().toggleProgress(true);
		getViewState().hideError();

		mRepository.weather().getWeather(mCity)
				.subscribeOn(WeatherAppSchedulers.io())
				.observeOn(WeatherAppSchedulers.mainThread())
				.subscribe(
						weather -> getViewState().showWeather(weather),
						throwable ->
						{
							getViewState().toggleProgress(false);
							getViewState().showError(throwable);
						},
						() -> getViewState().toggleProgress(false)
				);
	}
}
