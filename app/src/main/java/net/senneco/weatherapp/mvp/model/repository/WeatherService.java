package net.senneco.weatherapp.mvp.model.repository;

import net.senneco.weatherapp.app.WeatherApp;
import net.senneco.weatherapp.mvp.model.data.City;
import net.senneco.weatherapp.mvp.model.data.Location;
import net.senneco.weatherapp.mvp.model.data.Temp;
import net.senneco.weatherapp.mvp.model.data.Weather;
import net.senneco.weatherapp.mvp.model.network.WeatherApi;
import net.senneco.weatherapp.mvp.model.network.model.ForecastResponse;
import net.senneco.weatherapp.mvp.model.network.model.WeatherResponse;
import net.senneco.weatherapp.mvp.model.storage.WeatherStorage;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by senneco on 10.03.2016
 */
public class WeatherService
{
	@Inject
	WeatherStorage mStorage;
	@Inject
	WeatherApi mApi;

	WeatherService()
	{
		WeatherApp.getStorageComponent().inject(this);
	}

	public Observable<Weather> getWeather(City city)
	{
		return Observable.create((subscriber) ->
				{
					try
					{
						Weather cachedWeather = mStorage.getWeather(city);
						subscriber.onNext(cachedWeather);
					}
					catch (Exception e)
					{
						// ignore cache exception
					}


					WeatherResponse weatherResponse = null;
					ForecastResponse forecastResponse = null;

					try
					{
						if (city.getName() != null)
						{
							weatherResponse = mApi.getWeather(city.getName()).execute().body();
							forecastResponse = mApi.getForecast(city.getName()).execute().body();
						}
						else if (city.getLocation() != null)
						{
							Location location = city.getLocation();
							weatherResponse = mApi.getWeather(location.getLatitude(), location.getLongitude()).execute().body();
							forecastResponse = mApi.getForecast(location.getLatitude(), location.getLongitude()).execute().body();
						}
						else
						{
							throw new IllegalArgumentException("Both city name and location are unknown");
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
						subscriber.onError(e);
						return;
					}

					Weather weather = new Weather();
					weather.setCity(city);
					weather.getTemps().add(buildTemp(weatherResponse));
					for (WeatherResponse forecast : forecastResponse.getForecast())
					{
						weather.getTemps().add(buildTemp(forecast));
					}

					try
					{
						mStorage.saveWeather(weather);
					}
					catch (Exception e)
					{
						// ignore cache exception
					}

					subscriber.onNext(weather);

					subscriber.onCompleted();
				}
		);
	}

	private static Temp buildTemp(WeatherResponse weatherResponse)
	{
		Temp temp = new Temp();
		temp.setTimestamp(weatherResponse.getTimestamp() * 1000);
		temp.setTempMin(weatherResponse.getTemp().getTempMin());
		temp.setTemp(weatherResponse.getTemp().getTemp());
		temp.setTempMax(weatherResponse.getTemp().getTempMax());

		return temp;
	}
}
