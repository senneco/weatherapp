package net.senneco.weatherapp.mvp.model.repository;

/**
 * Created by senneco on 10.03.2016
 */
public class Repository
{
	private final CityService mCityService = new CityService();
	private final WeatherService mWeatherService = new WeatherService();

	public CityService cities()
	{
		return mCityService;
	}

	public WeatherService weather()
	{
		return mWeatherService;
	}
}
