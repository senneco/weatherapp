package net.senneco.weatherapp.mvp.view;

import java.util.Iterator;
import java.util.List;

import android.text.TextUtils;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.Pair;
import com.arellomobile.mvp.viewstate.ViewCommand;
import com.arellomobile.mvp.viewstate.strategy.StateStrategy;

/**
 * Created by senneco on 12.03.2016
 */
public class ClearStrategy implements StateStrategy
{
	@Override
	public <View extends MvpView> void beforeApply(List<Pair<ViewCommand<View>, Object>> list, Pair<ViewCommand<View>, Object> pair)
	{
		// pass
	}

	@Override
	public <View extends MvpView> void afterApply(List<Pair<ViewCommand<View>, Object>> list, Pair<ViewCommand<View>, Object> pair)
	{
		final Iterator<Pair<ViewCommand<View>, Object>> it = list.iterator();
		while (it.hasNext())
		{
			if (TextUtils.equals(it.next().first.getTag(), pair.first.getTag()))
			{
				it.remove();
			}
		}
	}
}
