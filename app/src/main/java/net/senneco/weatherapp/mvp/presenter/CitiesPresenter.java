package net.senneco.weatherapp.mvp.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import net.senneco.weatherapp.app.WeatherApp;
import net.senneco.weatherapp.mvp.common.WeatherAppSchedulers;
import net.senneco.weatherapp.mvp.model.data.City;
import net.senneco.weatherapp.mvp.model.repository.Repository;
import net.senneco.weatherapp.mvp.view.CitiesView;

import javax.inject.Inject;

/**
 * Created by senneco on 10.03.2016
 */
@InjectViewState
public class CitiesPresenter extends MvpPresenter<CitiesView>
{
	@Inject
	Repository mRepository;

	public CitiesPresenter()
	{
		WeatherApp.getAppComponent().inject(this);
	}

	@Override
	protected void onFirstViewAttach()
	{
		super.onFirstViewAttach();

		mRepository.cities().getCities()
				.subscribeOn(WeatherAppSchedulers.io())
				.observeOn(WeatherAppSchedulers.mainThread())
				.subscribe(cities -> getViewState().setCities(cities));
	}

	public void swipeItem(City city, int direction)
	{
		mRepository.cities().remove(city)
				.subscribeOn(WeatherAppSchedulers.io())
				.observeOn(WeatherAppSchedulers.mainThread())
				.subscribe(removedCity -> getViewState().removeCity(removedCity));
	}

	public void addCity(String name)
	{
		City city = new City();
		city.setName(name);

		mRepository.cities().add(city)
				.subscribeOn(WeatherAppSchedulers.io())
				.observeOn(WeatherAppSchedulers.mainThread())
				.subscribe(addedCity -> getViewState().addCity(addedCity));
	}
}
