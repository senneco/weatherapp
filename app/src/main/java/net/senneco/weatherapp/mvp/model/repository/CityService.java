package net.senneco.weatherapp.mvp.model.repository;

import java.util.List;

import net.senneco.weatherapp.app.WeatherApp;
import net.senneco.weatherapp.mvp.model.data.City;
import net.senneco.weatherapp.mvp.model.storage.CityStorage;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by senneco on 10.03.2016
 */
public class CityService
{
	@Inject
	CityStorage mStorage;

	CityService()
	{
		WeatherApp.getStorageComponent().inject(this);
	}

	public Observable<List<City>> getCities()
	{
		return Observable.create((subscriber) ->
				{
					subscriber.onNext(mStorage.getCites());
					subscriber.onCompleted();
				}
		);
	}

	public Observable<City> remove(City city)
	{
		return Observable.create((subscriber) ->
				{
					mStorage.remove(city);

					subscriber.onNext(city);
					subscriber.onCompleted();
				}
		);
	}

	public Observable<City> add(City city)
	{
		return Observable.create((subscriber) ->
				{
					mStorage.add(city);

					subscriber.onNext(city);
					subscriber.onCompleted();
				}
		);
	}
}
