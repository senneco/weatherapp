package net.senneco.weatherapp.mvp.model.storage.fake;

import java.util.ArrayList;
import java.util.List;

import net.senneco.weatherapp.mvp.model.data.City;
import net.senneco.weatherapp.mvp.model.storage.CityStorage;

/**
 * Created by senneco on 10.03.2016
 */
public class FakeCityStorage extends CityStorage
{
	private final List<City> mCities;

	public FakeCityStorage()
	{
		City london = new City();
		london.setName("London");

		City tokyo = new City();
		tokyo.setName("Tokyo");

		City newYork = new City();
		newYork.setName("New York");

		mCities = new ArrayList<>();
		mCities.add(london);
		mCities.add(tokyo);
		mCities.add(newYork);
	}

	@Override
	public List<City> getCites()
	{
		return new ArrayList<>(mCities);
	}

	@Override
	public void remove(City city)
	{
		mCities.remove(city);
	}

	@Override
	public void add(City city)
	{
		mCities.add(city);
	}
}
