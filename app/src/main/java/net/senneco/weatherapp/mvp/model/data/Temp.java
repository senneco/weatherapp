package net.senneco.weatherapp.mvp.model.data;

/**
 * Created by senneco on 10.03.2016
 */
public class Temp
{
	private double mTemp;
	private double mTempMin;
	private double mTempMax;
	private long mTimestamp;

	public double getTemp()
	{
		return mTemp;
	}

	public void setTemp(double temp)
	{
		mTemp = temp;
	}

	public double getTempMin()
	{
		return mTempMin;
	}

	public void setTempMin(double tempMin)
	{
		mTempMin = tempMin;
	}

	public double getTempMax()
	{
		return mTempMax;
	}

	public void setTempMax(double tempMax)
	{
		mTempMax = tempMax;
	}

	public long getTimestamp()
	{
		return mTimestamp;
	}

	public void setTimestamp(long timestamp)
	{
		mTimestamp = timestamp;
	}
}
