package net.senneco.weatherapp.mvp.model.storage.fake;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import net.senneco.weatherapp.mvp.model.data.City;
import net.senneco.weatherapp.mvp.model.data.Temp;
import net.senneco.weatherapp.mvp.model.data.Weather;
import net.senneco.weatherapp.mvp.model.storage.WeatherStorage;

/**
 * Created by senneco on 10.03.2016
 */
public class FakeWeatherStorage extends WeatherStorage
{
	@Override
	public Weather getWeather(City city)
	{
		Weather weather = new Weather();

		Temp temp = new Temp();
		temp.setTemp(8.02);
		temp.setTempMin(4.06);
		temp.setTempMax(10.0);
		temp.setTimestamp(new Date().getTime());
		weather.getTemps().add(temp);

		temp = new Temp();
		temp.setTemp(10.02);
		temp.setTempMin(6.06);
		temp.setTempMax(12.0);
		temp.setTimestamp(new Date().getTime() + TimeUnit.HOURS.toMillis(3));
		weather.getTemps().add(temp);

		temp = new Temp();
		temp.setTemp(12.02);
		temp.setTempMin(8.06);
		temp.setTempMax(14.0);
		temp.setTimestamp(new Date().getTime() + TimeUnit.HOURS.toMillis(6));
		weather.getTemps().add(temp);

		temp = new Temp();
		temp.setTemp(16.02);
		temp.setTempMin(12.06);
		temp.setTempMax(18.0);
		temp.setTimestamp(new Date().getTime() + TimeUnit.HOURS.toMillis(9));
		weather.getTemps().add(temp);

		return weather;
	}
}
