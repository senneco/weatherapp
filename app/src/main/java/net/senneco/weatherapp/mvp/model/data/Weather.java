package net.senneco.weatherapp.mvp.model.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by senneco on 10.03.2016
 */
public class Weather
{
	private City mCity;
	private List<Temp> mTemps;

	public Weather()
	{
		mTemps = new ArrayList<>();
	}

	public City getCity()
	{
		return mCity;
	}

	public void setCity(City city)
	{
		mCity = city;
	}

	public List<Temp> getTemps()
	{
		return mTemps;
	}

	public void setTemps(List<Temp> temps)
	{
		mTemps = temps;
	}
}
