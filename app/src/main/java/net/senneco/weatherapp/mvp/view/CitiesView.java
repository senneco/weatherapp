package net.senneco.weatherapp.mvp.view;

import java.util.List;

import com.arellomobile.mvp.MvpView;

import net.senneco.weatherapp.mvp.model.data.City;

/**
 * Created by senneco on 10.03.2016
 */
public interface CitiesView extends MvpView
{
	void setCities(List<City> cities);

	void removeCity(City city);

	void addCity(City city);
}
