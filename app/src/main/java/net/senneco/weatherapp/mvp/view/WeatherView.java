package net.senneco.weatherapp.mvp.view;

import com.arellomobile.mvp.MvpView;

import net.senneco.weatherapp.mvp.model.data.Weather;

/**
 * Created by senneco on 10.03.2016
 */
public interface WeatherView extends MvpView
{
	void showWeather(Weather weather);

	void showError(Throwable throwable);

	void hideError();

	void toggleProgress(boolean isVisible);
}
