package net.senneco.weatherapp.mvp.model.network.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

/**
 * Created by senneco on 11.03.2016
 */
public class ForecastResponse
{
	@SerializedName("list")
	private List<WeatherResponse> mForecast;

	public List<WeatherResponse> getForecast()
	{
		return mForecast;
	}
}
