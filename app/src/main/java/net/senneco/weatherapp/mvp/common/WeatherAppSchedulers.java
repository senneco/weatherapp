package net.senneco.weatherapp.mvp.common;

import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by senneco on 10.03.2016
 */
public class WeatherAppSchedulers
{
	public static Scheduler io()
	{
		return Schedulers.io();
	}

	public static Scheduler computation()
	{
		return Schedulers.computation();
	}

	public static Scheduler immediate()
	{
		return Schedulers.immediate();
	}

	public static Scheduler trampoline()
	{
		return Schedulers.trampoline();
	}

	public static Scheduler mainThread()
	{
		return AndroidSchedulers.mainThread();
	}
}
