package net.senneco.weatherapp.di.module;

import net.senneco.weatherapp.mvp.model.storage.CityStorage;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by senneco on 10.03.2016
 */
@Module
public class CityModule
{
	private CityStorage mStorage;

	public CityModule(CityStorage storage)
	{
		mStorage = storage;
	}

	@Provides
	@Singleton
	public CityStorage provideCitiesStorage()
	{
		return mStorage;
	}
}
