package net.senneco.weatherapp.di.module;

import net.senneco.weatherapp.mvp.model.repository.Repository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by senneco on 10.03.2016
 */
@Module
public class RepositoryModule
{
	@Provides
	@Singleton
	public Repository provideRepository() {
		return new Repository();
	}
}
