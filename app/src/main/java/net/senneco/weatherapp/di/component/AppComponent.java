package net.senneco.weatherapp.di.component;

/**
 * Created by senneco on 10.03.2016
 */

import android.content.Context;

import net.senneco.weatherapp.di.module.AppModule;
import net.senneco.weatherapp.mvp.model.repository.Repository;
import net.senneco.weatherapp.mvp.model.storage.CityStorage;
import net.senneco.weatherapp.mvp.presenter.CitiesPresenter;
import net.senneco.weatherapp.mvp.presenter.WeatherPresenter;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent
{
	Context context();

	Repository repository();

	void inject(CitiesPresenter citiesPresenter);
	void inject(WeatherPresenter weatherPresenter);
	void inject(CityStorage cityStorage);
}
