package net.senneco.weatherapp.di.module;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by senneco on 10.03.2016
 */
@Module(includes = {RepositoryModule.class})
public class AppModule
{
	private Application mApplication;

	public AppModule(Application application)
	{
		mApplication = application;
	}

	@Provides
	@Singleton
	public Context provideApplicationContext()
	{
		return mApplication.getApplicationContext();
	}
}
