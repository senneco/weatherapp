package net.senneco.weatherapp.di.module;

import java.lang.reflect.Field;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.FieldNamingStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by senneco on 11.03.2016
 */
@Module
public class NetworkModule
{
	@Provides
	@Singleton
	Gson provideGson()
	{
		class CustomFieldNamingPolicy implements FieldNamingStrategy
		{
			@Override
			public String translateName(Field field) {
				String name = FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES.translateName(field);
				name = name.substring(2, name.length()).toLowerCase();
				return name;
			}
		}

		return new GsonBuilder()
				.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
				.setFieldNamingStrategy(new CustomFieldNamingPolicy())
				.setPrettyPrinting()
				.serializeNulls()
				.create();
	}

	@Provides
	@Singleton
	GsonConverterFactory provideConverterFactory(Gson gson)
	{
		return GsonConverterFactory.create(gson);
	}

	@Provides
	@Singleton
	Retrofit.Builder provideRestAdapter(GsonConverterFactory converter)
	{
		HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
		interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
		OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

		return new Retrofit.Builder()
				.client(client)
				.addConverterFactory(converter);
	}
}
