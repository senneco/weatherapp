package net.senneco.weatherapp.di.module;

import net.senneco.weatherapp.mvp.model.network.WeatherApi;
import net.senneco.weatherapp.mvp.model.storage.WeatherStorage;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by senneco on 10.03.2016
 */
@Module(includes = {NetworkModule.class})
public class WeatherModule
{
	private WeatherStorage mStorage;

	public WeatherModule(WeatherStorage storage)
	{
		mStorage = storage;
	}

	@Provides
	@Singleton
	public WeatherStorage provideWeatherStorage()
	{
		return mStorage;
	}

	@Provides
	@Singleton
	public WeatherApi provideWeatherApi(Retrofit.Builder adapterBuilder)
	{
		return adapterBuilder
				.baseUrl("http://api.openweathermap.org/")
				.build().create(WeatherApi.class);
	}
}
