package net.senneco.weatherapp.di.component;

import net.senneco.weatherapp.di.module.CityModule;
import net.senneco.weatherapp.di.module.WeatherModule;
import net.senneco.weatherapp.mvp.model.repository.CityService;
import net.senneco.weatherapp.mvp.model.repository.WeatherService;
import net.senneco.weatherapp.mvp.model.storage.CityStorage;
import net.senneco.weatherapp.mvp.model.storage.WeatherStorage;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by senneco on 10.03.2016
 */
@Singleton
@Component(modules = {CityModule.class, WeatherModule.class})
public interface StorageComponent
{
	CityStorage citiesStorage();

	WeatherStorage weatherStorage();

	void inject(CityService service);
	void inject(WeatherService service);
}
