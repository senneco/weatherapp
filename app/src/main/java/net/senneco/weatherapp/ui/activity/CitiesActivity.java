package net.senneco.weatherapp.ui.activity;

import java.util.List;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.InputType;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.tbruyelle.rxpermissions.RxPermissions;

import net.senneco.weatherapp.R;
import net.senneco.weatherapp.mvp.model.data.City;
import net.senneco.weatherapp.mvp.presenter.CitiesPresenter;
import net.senneco.weatherapp.mvp.view.CitiesView;
import net.senneco.weatherapp.ui.adapter.CitiesAdapter;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CitiesActivity extends MvpAppCompatActivity implements CitiesView, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
{
	@InjectPresenter
	CitiesPresenter mCitiesPresenter;

	@Bind(R.id.content_cities_relative_layout_current_location)
	RelativeLayout mCurrentLocationRelativeLayout;
	@Bind(R.id.content_cities_text_view_current_location)
	TextView mCurrentLocationTextView;
	@Bind(R.id.content_cities_button_refresh_current_location)
	View mRefreshCurrentLocationButton;
	@Bind(R.id.content_cities_recycler_view_cities)
	RecyclerView mCitiesView;
	@Bind(R.id.content_cities_button_add_city)
	FloatingActionButton mAddButton;

	private CitiesAdapter mCitiesAdapter;
	private LinearLayoutManager mCitiesLayoutManager;
	private GoogleApiClient mGoogleApiClient;
	private Location mCurrentLocation;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cities);

		ButterKnife.bind(this);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		mAddButton.setOnClickListener(view -> showAddingDialog());

		mCitiesAdapter = new CitiesAdapter(city -> startActivity(CityActivity.getIntent(this, city)));
		mCitiesLayoutManager = new LinearLayoutManager(this);
		ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT)
		{
			@Override
			public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target)
			{
				return false;
			}

			@Override
			public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction)
			{
				mCitiesPresenter.swipeItem(mCitiesAdapter.getCity(viewHolder.getAdapterPosition()), direction);
			}
		};

		mCitiesView.setAdapter(mCitiesAdapter);
		mCitiesView.setHasFixedSize(true);
		mCitiesView.setLayoutManager(mCitiesLayoutManager);

		new ItemTouchHelper(simpleCallback).attachToRecyclerView(mCitiesView);

		mRefreshCurrentLocationButton.setOnClickListener(view -> requestCurrentLocation());
		mCurrentLocationRelativeLayout.setOnClickListener(view -> {
			if (mCurrentLocation != null)
			{
				City city = new City();
				city.setLocation(mCurrentLocation);
				startActivity(CityActivity.getIntent(this, city));
			}
			else
			{
				Toast.makeText(this, "Unknown current location", Toast.LENGTH_LONG).show();
			}
		});

		if (mGoogleApiClient == null)
		{
			mGoogleApiClient = new GoogleApiClient.Builder(this)
					.addConnectionCallbacks(this)
					.addOnConnectionFailedListener(this)
					.addApi(LocationServices.API)
					.build();
		}
	}

	private void showAddingDialog()
	{
		new MaterialDialog.Builder(this)
				.title(R.string.add_city)
				.inputType(InputType.TYPE_CLASS_TEXT)
				.input(R.string.add_city, 0, (dialog, input) -> mCitiesPresenter.addCity(input.toString()))
				.show();
	}

	@Override
	public void setCities(List<City> cities)
	{
		mCitiesAdapter.setItems(cities);
	}

	@Override
	public void removeCity(City city)
	{
		mCitiesAdapter.removeCity(city);
	}

	@Override
	public void addCity(City city)
	{
		mCitiesAdapter.addCity(city);

		if (!mCitiesPresenter.isInRestoreState(this))
		{
			if (mCitiesLayoutManager.findFirstCompletelyVisibleItemPosition() == 0)
			{
				mCitiesLayoutManager.scrollToPosition(0);
			}
		}
	}

	public void updateCurrentLocation()
	{
		if (!mGoogleApiClient.isConnected())
		{
			Toast.makeText(this, R.string.google_service_not_connected, Toast.LENGTH_SHORT).show();
			return;
		}

		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
		{
			Toast.makeText(this, R.string.location_define_permission_denied, Toast.LENGTH_SHORT).show();
			return;
		}

		mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
		if (mCurrentLocation != null)
		{
			mCurrentLocationTextView.setText(getString(R.string.current_location_lat_lng, mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()));
		}
		else
		{
			mCurrentLocationTextView.setText(R.string.unknown_location);
		}
	}


	@Override
	protected void onStart()
	{
		mGoogleApiClient.connect();

		super.onStart();
	}

	@Override
	protected void onStop()
	{
		mGoogleApiClient.disconnect();

		super.onStop();
	}

	@Override
	public void onConnected(@Nullable Bundle bundle)
	{
		requestCurrentLocation();
	}

	private void requestCurrentLocation()
	{
		RxPermissions.getInstance(this)
				.request(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
				.subscribe(granted -> {
					updateCurrentLocation();
				});
	}

	@Override
	public void onConnectionSuspended(int i)
	{
		// pass
	}

	@Override
	public void onConnectionFailed(@NonNull ConnectionResult connectionResult)
	{
		// pass
	}
}
