package net.senneco.weatherapp.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.f2prateek.dart.Dart;
import com.f2prateek.dart.InjectExtra;

import net.senneco.weatherapp.R;
import net.senneco.weatherapp.mvp.model.data.City;
import net.senneco.weatherapp.mvp.model.data.Weather;
import net.senneco.weatherapp.mvp.presenter.WeatherPresenter;
import net.senneco.weatherapp.mvp.view.WeatherView;
import net.senneco.weatherapp.ui.adapter.TempsAdapter;

import org.parceler.Parcels;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CityActivity extends MvpAppCompatActivity implements WeatherView
{
	private static final String EXTRA_CITY = "city";

	@InjectPresenter
	WeatherPresenter mWeatherPresenter;

	@Bind(R.id.activity_city_coordinator_layout)
	CoordinatorLayout mCoordinatorLayout;
	@Bind(R.id.content_city_list_view_weather)
	ListView mWeatherListView;

	@InjectExtra(EXTRA_CITY)
	City mCity;

	private TempsAdapter mTempsAdapter;
	private Snackbar mSnackbar;

	public static Intent getIntent(Context context, City city)
	{
		Intent intent = new Intent(context, CityActivity.class);
		intent.putExtra(EXTRA_CITY, Parcels.wrap(city));

		return intent;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_city);

		Dart.inject(this);
		ButterKnife.bind(this);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

		setSupportActionBar(toolbar);
		//noinspection ConstantConditions
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle(mCity.getName() != null ? mCity.getName() : getString(R.string.current_location_lat_lng, mCity.getLocation().getLatitude(), mCity.getLocation().getLongitude()));
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setCustomView(LayoutInflater.from(this).inflate(R.layout.item_progress, toolbar, false));

		mWeatherPresenter.init(mCity);

		mTempsAdapter = new TempsAdapter();
		mWeatherListView.setAdapter(mTempsAdapter);

		mSnackbar = Snackbar.make(mCoordinatorLayout, R.string.error_message, Snackbar.LENGTH_INDEFINITE).setAction(R.string.try_again, view -> mWeatherPresenter.loadData());
	}

	@Override
	public void showWeather(Weather weather)
	{
		mTempsAdapter.setTemps(weather.getTemps());
	}

	@Override
	public void showError(Throwable throwable)
	{
		if (!mSnackbar.isShown())
		{
			mSnackbar.show();
		}
	}

	@Override
	public void hideError()
	{
		if (mSnackbar.isShown())
		{
			mSnackbar.dismiss();
		}
	}

	@Override
	public void toggleProgress(boolean isVisible)
	{
		//noinspection ConstantConditions
		getSupportActionBar().getCustomView().setVisibility(isVisible ? View.VISIBLE : View.GONE);
	}
}
