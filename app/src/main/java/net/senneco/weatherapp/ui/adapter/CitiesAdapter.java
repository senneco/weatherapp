package net.senneco.weatherapp.ui.adapter;

import java.util.ArrayList;
import java.util.List;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.senneco.weatherapp.R;
import net.senneco.weatherapp.mvp.model.data.City;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by senneco on 10.03.2016
 */
public class CitiesAdapter extends RecyclerView.Adapter<CitiesAdapter.Holder>
{
	private final ArrayList<City> mCities;
	private OnCityClickListener mClickListener;

	public CitiesAdapter(OnCityClickListener clickListener)
	{
		mClickListener = clickListener;
		mCities = new ArrayList<>(0);
	}

	public void setItems(List<City> items)
	{
		mCities.clear();
		mCities.addAll(items);

		notifyDataSetChanged();
	}

	public void addCity(City city)
	{
		mCities.add(0, city);

		notifyItemInserted(0);
	}

	public void removeCity(City city)
	{
		int removePosition = mCities.indexOf(city);
		if (removePosition < 0)
		{
			return;
		}

		mCities.remove(removePosition);

		notifyItemRemoved(removePosition);
	}

	@Override
	public Holder onCreateViewHolder(ViewGroup parent, int viewType)
	{
		return new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_city, parent, false));
	}

	@Override
	public void onBindViewHolder(Holder holder, int position)
	{
		holder.bind(mCities.get(position));
	}

	@Override
	public int getItemCount()
	{
		return mCities.size();
	}

	public City getCity(int position)
	{
		return mCities.get(position);
	}

	public class Holder extends RecyclerView.ViewHolder
	{
		@Bind(R.id.item_city_text_view_name)
		TextView mNameTextView;
		private City mCity;

		public Holder(View itemView)
		{
			super(itemView);

			ButterKnife.bind(this, itemView);

			itemView.setOnClickListener(view -> mClickListener.onCityClick(mCity));
		}

		public void bind(City city)
		{
			mCity = city;
			mNameTextView.setText(city.getName());
		}
	}

	public interface OnCityClickListener
	{
		void onCityClick(City city);
	}
}
