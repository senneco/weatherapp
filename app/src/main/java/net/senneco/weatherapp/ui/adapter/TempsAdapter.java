package net.senneco.weatherapp.ui.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import net.senneco.weatherapp.R;
import net.senneco.weatherapp.mvp.model.data.Temp;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by senneco on 11.03.2016
 */
public class TempsAdapter extends BaseAdapter
{
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	private List<Temp> mTemps;

	public TempsAdapter()
	{
		mTemps = new ArrayList<>();
	}

	public void setTemps(List<Temp> temps)
	{
		mTemps.clear();
		mTemps.addAll(temps);

		notifyDataSetChanged();
	}

	@Override
	public int getCount()
	{
		return mTemps.size();
	}

	@Override
	public Temp getItem(int position)
	{
		return mTemps.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		Holder holder;
		if (convertView == null)
		{
			convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_temp, parent, false);

			holder = new Holder(convertView);

			convertView.setTag(holder);
		}
		else
		{
			holder = (Holder) convertView.getTag();
		}

		holder.bind(getItem(position));

		return convertView;
	}

	static class Holder
	{
		@Bind(R.id.item_temp_text_view_title)
		TextView mTitleTextView;
		@Bind(R.id.item_temp_text_view_temp_min)
		TextView mTempMinTextView;
		@Bind(R.id.item_temp_text_view_temp)
		TextView mTempTextView;
		@Bind(R.id.item_temp_text_view_temp_max)
		TextView mTempMaxTextView;

		Holder(View itemView)
		{
			ButterKnife.bind(this, itemView);
		}

		void bind(Temp temp)
		{
			Context context = mTitleTextView.getContext();

			mTitleTextView.setText(DATE_FORMAT.format(temp.getTimestamp()));

			mTempMinTextView.setText(context.getString(R.string.temp, temp.getTempMin()));
			mTempTextView.setText(context.getString(R.string.temp, temp.getTemp()));
			mTempMaxTextView.setText(context.getString(R.string.temp, temp.getTempMax()));
		}
	}
}
